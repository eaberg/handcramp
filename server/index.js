const express = require("express");
const socketIO = require("socket.io");
const path = require("path");
const Sentry = require("@sentry/node");

Sentry.init({
	dsn: "https://34ba3fad20a24955bc65dc9c2595ed3e@sentry.io/1391576"
});

const PORT = process.env.PORT || 3000;
const INDEX = path.join(__dirname, "../dist/index.html");

const clientHandler = require("./clientHandler");
const gameHandler = require("./gameHandler");
const roomHandler = require("./roomHandler");

const server = express()
	.use(Sentry.Handlers.requestHandler())
	.use(express.static("dist"))
	.get("/.well-known/pki-validation/", (req, res) => {
		res.sendFile(path.join(__dirname, "80DAE963282F0DD74208E314FD7FA9D4.txt"));
	})
	.all("*", (req, res) => {
		res.sendFile(INDEX);
	})
	.use(Sentry.Handlers.errorHandler())
	.listen(PORT, () => console.log(`Listening on ${PORT}`));

const io = socketIO(server);

io.on("connection", function(socket) {
	const client = clientHandler(socket, io);

	client.on("close", () => error("timeout"));

	const { createRoom, joinRoom, joinRoomComputer, error } = roomHandler(
		client,
		io
	);
	const { selectHand, replay, roundEnd } = gameHandler(client, io);

	client.on("createRoom", createRoom);

	client.on("joinRoom", joinRoom);

	client.on("joinRoomCompuer", joinRoomComputer);

	client.on("disconnecting", () => error("disconnecting"));

	client.on("disconnect", () => error("timeout"));

	client.on("connect_timeout", () => error("timeout"));

	client.on("connect_error", () => error("error"));

	client.on("disconnect", () => error("error"));

	client.on("replay", replay);

	client.on("selectHand", selectHand);

	client.on("roundEnd", roundEnd);
});
